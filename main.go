package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/streadway/amqp"

	"gitlab.com/sangha/mq"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func main() {
	conn, err := amqp.Dial("amqp://quitty:quitty@10.0.3.135:5672/rabbit")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	msgs, err := ch.Consume(
		"receipts", // queue
		"",         // consumer
		false,      // auto-ack
		false,      // exclusive
		false,      // no-local
		false,      // no-wait
		nil,        // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			rcpt := mq.Receipt{}
			json.Unmarshal(d.Body, &rcpt)
			log.Printf("Received a message: %+v", rcpt)
			log.Printf("Done")
			d.Ack(false)
		}
	}()

	log.Println("Waiting for messages. To exit press CTRL+C")
	<-forever
}
